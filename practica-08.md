# Práctica 10

## Incorporar Flyway a bases de datos existentes

Es posible incorporar el versionado de esquema con Flyway a bases de datos que ya tenemos en ambientes de producción y para ello tenemos el comando ``flyway baseline``.

## Comando Baseline

Para aplicarlo, podemos realizar los siguientes pasos:

1. Configurar la conexión a la base de datos existente.

2. El primer comando a ejecutar sería ``flyway baseline`` el cual creará la tabla de migraciones en la base de datos y pondrá como versión actual de la base de datos el "1".

3. Los siguientes cambios a la estructura de la base se guardarían en scripts SQL de migraciones. El primer script que vayamos a crear debemos nombrarlo con la versión "2". Por ejemplo: **"V002__Creacion_de_tabla.sql"**

4. Es recomendable deshabilitar el comando ``flyway clean`` y tener un respaldo de la base de datos.
