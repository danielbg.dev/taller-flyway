# Práctica 6
## Limpieza de la base de datos

El comando para limpiar la base de datos es ``flyway clean``, este borrará todos los objetos sin eliminar el esquema y es muy útil para empezar de cero.

Es recomendable bloquar su ejecución en ambientes de producción por obvias razones. Esto se logra mediante una configuración en el archivo ``conf/flyway.conf``

## Ciclo info-migrate-clean
Son los 3 comandos básicos que usarás comunmente con Flyway. En la práctica 5, utilizamos ``info`` y ``migrate`` para consultar la tabla de migraciones y para aplicar cambios. En esta práctica utilizaremos ``clean``.

1. Ejecuta ``flyway info`` y asegúrate de tener aplicadas las migraciones creadas en la práctica 5. Todas las migraciones deberán tener el estatus **success**.

2. En la línea de comandos ejecuta el comando ``flyway clean``

3. Observa que la tabla de migraciones muestre todas las migraciones con estatus **pending**

## Bloqueo del comando clean en ambientes de producción

1. Abre el archivo ``flyway.conf`` y ubica la siguiente línea:

```
# flyway.cleanDisabled=
```
2. Para deshabilitar el comando clean quita el símbolo \# al inicio de la línea y coloca el valor ``true`` para indicarle a Flyway que ignore al comando ``clean``

```
flyway.cleanDisabled=true
```

3. Vuelve a la línea de comandos y aplica las migraciones con ``flyway migrate``

4. Comprueba que se hayan aplicado con el comando ``flyway info``

5. Intenta ejecutar el comando ``flyway clean`` Deberá aparecer un mensaje de error indicando que el comando ha sido deshabilitado.

6. Vuelve al archivo de configuración ``flyway.conf`` y habilita nuevamente el comando clean, poniendo el valor de ``false`` en el parámetro ``flyway.cleanDisabled``.

```
flyway.cleanDisabled=false
```

7. Vuelve a la línea de comando y **comprueba** que el comando funciona nuevamente. Ya no se debería presentar el mensaje de error al correr ``flyway clean``.
