# Práctica 4
## Establecer una conexión

1. En tu manejador de bases de datos, crea una nueva base de datos llamada **"taller01fw"**

2. Copia el software descargado y desempacado del directorio `taller-flyway/software/flyway-7.7.0` al directorio `proyecto-01` Quedará de la siguiente forma:

```
- taller-flyway
  - proyecto-01
    - flyway-7.7.0
```

3. Abre una línea de comandos y ubicate en el directorio `taller-flyway / proyecto-01 / flyway-7.7.0` y ejecuta el siguiente commando:

```shell
flyway info
```

- El comando mostrará un mensaje de error en color rojo que indica que no es posible establecer conexión a la BD y nos sugiere que configuremos una URL, nombre de usuario (user) y contraseña (password)

4. Configura la conexión

  4.1. Abre el archivo `taller-flyway / proyecto-01 / flyway-7.7.0 / conf / flyway.conf` en tu editor de texto plano o en tu editor de código.

  4.2. Observa los ejemplos de la sección **"JDBC url to use to connect to the database"** y encuentra el que corresponda a tu manejador de bases de datos.

  4.3. Al final de los ejemplos encontrarás la línea

  ``# flyway.url=``

  Quita el símbolo \# al inicio de la línea y ahora **copia el ejemplo de conexión que corresponda a tu base de datos**. Por ejemplo, para establecer una conexión con Postgres la línea queda así:

  ``flyway.url=jdbc:postgresql://<host>:<port>/<database>``

  Observa que la línea original es más larga...

  ``jdbc:postgresql://<host>:<port>/<database>?<key1>=<value1>&<key2>=<value2>...``

  Pero los parámetros después del símbolo ``?`` son opcionales, al menos para una instalación estándar de Postgres. Tu manejador podría requerir parámetros más, parámetros menos de los que se indican en los ejemplos.

  4.4. Ahora vamos a **reemplazar los parámetros** correspondientes al servidor ``<host>``, puerto ``<port>`` y nombre de la base de datos ``<database>`` de la cadena de conexión por los valores adecuados a tu ambiente. Por ejemplo:

  ``jdbc:postgresql://127.0.0.1:5432/taller01fw``

  - Observa que el nombre de la base de datos es la que hemos creado al inicio de esta práctica.
  - Este ejemplo corresponde a mi ambiente. Tengo Postgres corriendo en mi equipo, por eso la dirección es la tradicional de localhost.
  - Mi servidor de Postgres está escuchando en el puerto 5432.

5. Una vez configurado el URL, vuelve a ejecutar el comando ``flyway info`` ¿Qué sucede?

Observa que FlywayDB te pedirá que ingreses el nombre de usuario y contraseña para establecer la conexión. Esto depende de la configuración de tu manejador de bases de datos.

Ingresa tu nombre de usuario y contraseña, en caso de ser correctos Flyway mostrará la **tabla de migraciones** vacía. En caso contrario, verías un mensaje de error indicando que las credenciales son incorrectas.

6. **Configura el nombre de usuario y contraseña.** Vuelve al archivo de configuración de Flyway y coloca tu nombre de usuario y contraseña en los campos correspondientes de las líneas:

```
# flyway.user=
# flyway.password=
```
No olvides quitar el primer símbolo. Por ejemplo:

```
flyway.user=postgres
flyway.password=s3cret
```
7. Vuelve a ejecutar ``flyway info`` se deberá mostrar la tabla de migraciones, ahora sin pedir nombre de usuario y contraseña.

**Comprueba** que la tabla de migraciones indique los valores correctos en la línea "Database". Deben coincidir el URL, servidor, puerto y nombre de la base de datos que has configurado.
